import java.util.ArrayList;


public class History {

	private ArrayList<HistoryLog> history = new ArrayList<HistoryLog>();
	
	public void addLog(HistoryLog log)
	{
		history.add(log);
	}
	public ArrayList<HistoryLog> getHistory()
	{
		return history;
	}
	public HistoryLog getLastOperation()
	{
		if (history.size()>=1)
		return history.get(history.size()-1);
		else return null;
	}
	public void printHistory()
	{
		for (HistoryLog tmpLog : history)
		{
			System.out.println(tmpLog.toString());
		}
	}
}
