import java.util.ArrayList;


public class Bank {

	private ArrayList<Account> accounts = new ArrayList<Account>();
	private double overAllAmount = 0;
	
	public void income(Account inAccount, double amount)
	{
		Income income = new Income(inAccount, amount);
		income.execute();
		addOverAllAmount(amount);
	}
	public void outcome(Account inAccount, double amount)
	{
		Outcome outcome = new Outcome(inAccount, amount);
		if(outcome.execute()) addOverAllAmount(-amount);
	}
	public void transfer(Account fromAccount, Account toAccount, double amount)
	{
		Transfer transfer = new Transfer(fromAccount, toAccount, amount);
		transfer.execute();
	}
	public ArrayList<Account> getAccounts() {
		return accounts;
	}
	public void addAccount(Account account)
	{
		accounts.add(account);
	}
	public void deleteAccount(Account account)
	{
		accounts.remove(account);
	}
	public double getOverAllAmount() {
		return overAllAmount;
	}
	public void addOverAllAmount(double amount)
	{
		overAllAmount += amount;
	}
	public String toString()
	{
		return "Liczba kont w banku " + accounts.size() + "  || ilosc pieniedzy w banku " + overAllAmount + " PLN";
	}
}
