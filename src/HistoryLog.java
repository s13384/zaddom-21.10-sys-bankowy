import java.util.Date;


public class HistoryLog {

	private Date dateOfOperation;
	private String title;
	private OperationClass operationClass;
	private double amount;
	
	HistoryLog(String title, OperationType operationType, double amount)
	{
		this.title = title;
		this.dateOfOperation = new Date();
		this.amount = amount;
		this.operationClass = new OperationClass(operationType);
		
	}
	HistoryLog(String title, OperationType operationType, double amount, String who)
	{
		this.title = title;
		this.dateOfOperation = new Date();
		this.amount = amount;
		this.operationClass = new OperationClass(operationType, who);
		
	}
	
	public Date getDateOfOpration() {
		return dateOfOperation;
	}
	public String getTitle() {
		return title;
	}
	public double getAmount(){
		return amount;
	}
	public OperationClass getOperationClass() {
		return operationClass;
	}
	public String getOperationClassInfo() {
		return operationClass.toString();
	}
	public String toString()
	{ 
		return "Tytul : " + title +" || Typ operacji : " + operationClass.toString() + " || Kwota " + amount + " || Data operacji : " + dateOfOperation ;
	}
}
