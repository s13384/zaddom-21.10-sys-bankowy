
public class Main {
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Bank bank = new Bank();
		
		bank.addAccount(new Account("Wladek Kowalski"));
		bank.addAccount(new Account("Juliusz Cezar"));
		bank.addAccount(new Account("Huanito Rodriquez"));
		
		bank.income(bank.getAccounts().get(0), 1000.15);
		bank.income(bank.getAccounts().get(1), 200.47);
		bank.income(bank.getAccounts().get(2), 500.99);
		
		printBankAccounts(bank);
		
		bank.transfer(bank.getAccounts().get(0), bank.getAccounts().get(2), 1200);
		bank.transfer(bank.getAccounts().get(0), bank.getAccounts().get(2), 400);
		
		printBankAccounts(bank);
		
		bank.outcome(bank.getAccounts().get(2),100);
		
		printBankAccounts(bank);
		
		bank.getAccounts().get(0).printHistory();
		bank.getAccounts().get(1).printHistory();
		bank.getAccounts().get(2).printHistory();
	}
	public static void printBankAccounts(Bank bank)
	{
		System.out.println("--------------------------------------");
		System.out.println(bank.toString());
		for (Account tmpAcc : bank.getAccounts())
		{
			System.out.println(tmpAcc.toString());
		}
		System.out.println("--------------------------------------");
	}

}
