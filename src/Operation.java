
public abstract class Operation {

	String name;
	
	abstract boolean execute();
	
}
