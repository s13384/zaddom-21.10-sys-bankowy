
public class Transfer extends Operation {

	Account fromAccount;
	Account toAccount;
	double amount;
	
	Transfer(Account fromAccount , Account toAccount, double amount)
	{
		this.fromAccount = fromAccount;
		this.toAccount = toAccount;
		this.amount = amount;
	}
	@Override
	boolean execute() {
		
		if (fromAccount.substract(amount))
		{
			toAccount.add(amount);
			addLog(fromAccount, OperationType.outcome, toAccount.getName());
			addLog(toAccount, OperationType.income, fromAccount.getName());
			
			System.out.println("BankingSystem : " + fromAccount.getName() + "->" + fromAccount.getNumber() +
					" przekazuje " +
					toAccount.getName() + "->" + toAccount.getNumber() + " " + amount + " PLN");
			return true;
		}
		else 
		{
			System.out.println("BankingSystem : " + fromAccount.getName() + "->" + fromAccount.getNumber() +
							" niema wystarczajaco duzo srodkow by przekazac " +
							toAccount.getName() + "->" + toAccount.getNumber() + " " + amount + " PLN");
		}
		return false;
		
	}
	private void addLog(Account account, OperationType type, String name)
	{
		account.addLog(new HistoryLog(" ", type, amount, name));
	}

}
