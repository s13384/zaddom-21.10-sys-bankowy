
public class Outcome extends Operation {

	Account account;
	double amount;
	
	Outcome(Account account, double amount)
	{
		this.account = account;
		this.amount = amount;
	}
	@Override
	boolean execute() {
		
		if (account.substract(amount))
		{
		addLog(account);
		return true;
		}
		return false;
		
	}
	private void addLog(Account account)
	{
		account.addLog(new HistoryLog(" ", OperationType.outcome, amount));
	}
}
