
public class Income extends Operation{

	Account account;
	double amount;
	
	Income(Account account, double amount)
	{
		this.account = account;
		this.amount = amount;
	}
	@Override
	boolean execute() {
		
		account.add(amount);
		addLog(account);
		
		System.out.println("BankingSystem : " + account.getName() + "->" + account.getNumber() +
				" otrzymuje " + amount + " PLN");
		return true;
		
	}
	private void addLog(Account account)
	{
		account.addLog(new HistoryLog(" ", OperationType.income, amount));
	}
	

}
