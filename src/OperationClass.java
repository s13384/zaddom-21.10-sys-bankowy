
public class OperationClass {

	private OperationType type;
	private String who;
	
	OperationClass(OperationType type, String who)
	{
		this.type = type;
		this.who = who;
	}
	OperationClass(OperationType type)
	{
		this.type = type;
		
		if (type==OperationType.income)
		{
			this.who = "Wplata";
		}
		else this.who = "Wyplata";
	}
	public OperationType getType() {
		return type;
	}
	public void setType(OperationType type) {
		this.type = type;
	}
	public String getWho() {
		return who;
	}
	public void setWho(String who) {
		this.who = who;
	}
	public String toString()
	{
		if(who=="Wplata" || who=="Wyplata")
			return who;
		
		else if (type==OperationType.income)
			return "Wplata od " + who;
		else 
			return "Wyplata do " + who;
	}
}
